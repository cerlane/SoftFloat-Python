# SoftFloat-Python

This is the python wrapper (using SWIG) of Berkeley SoftFloat (modified version of Release 3d).

Currently we support only 64-bit systems:

Wheels support:

* Windows (Python 3.5-3.7)
* OS-X (Python 2.7 and 3.7)
* Linux (Python 2.7, 3.4-3.7)

For all other versions, you will have to compile the source as a part of pip installation. Consequently for windows and OS-X users, please install Visual Studio Build Tools and Xcode respectively.
