/*
Author: S.H. Leong (Cerlane)

Copyright (c) 2018 Next Generation Arithmetic

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


%module softfloat
%{
#include "softfloat.h"
%}


%include "softfloat_types.h"
%include "softfloat.h"
%include <stdint.i>

%pythonbegin %{
from __future__ import absolute_import, print_function, division
import struct
%}


%pythoncode %{

def convertToColor(i, fs, es):
   orig="{0:b}".format(i).zfill(fs)
   m=0
   regime=1
   exponent=0
   firstFrac = 1
   colored=""
   if es>0:
       exponent = 1
       e=0
   for c in orig:
       if m==0:
           colored+="\033[1;37;41m"+c+"\033[0m"              
       elif exponent==1:
           if e==0:
              colored+="\033[1;37;44m"+c
           else:
              colored+=c
           e+=1
           if e==es:
              colored+="\033[0m"
              exponent = 0
       else:
           if firstFrac==1:
              colored+="\033[1;37;40m"+c
              firstFrac=0
           else:
              colored+=c
       m+=1
       if (m!=fs and m%8==0):
           colored+=" "

   return colored+"\033[0m"
       
class float64:
   def __init__(self, value=None, bits=None):
       try:
           if bits is not None:
               if isinstance(bits, (int)):
                   self.v = float64_t()
                   self.v.v = bits
               else:
                   raise Exception("Bits can only be set with integer values")
           else:
               if value is None:
                   self.v = float64_t()
                   self.v.v = 0
               elif isinstance(value, (int)):
                   self.v = _softfloat.i64_to_f64(value)
               else:
                   self.v = _softfloat.convertDoubleToF64(value)
       except Exception as error:
           print(repr(error))
   def type(self):
       return 'float64'
   def __add__(self, other):
       try:
          a = float64(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f64_add(self.v, _softfloat.i64_to_f64(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f64_add(self.v, _softfloat.convertDoubleToF64(other))
          else:           
              a.v = _softfloat.f64_add(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for +: float64 and ",other.type())          
   def __radd__(self, other):
       return self.__add__(other)  
   def __sub__(self, other):
       try:
          a = float64(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f64_sub(self.v, _softfloat.i64_to_f64(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f64_sub(self.v, _softfloat.convertDoubleToF64(other))
          else:
              a.v = _softfloat.f64_sub(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for -: float64 and ",other.type())      
   def __rsub__(self, other):
       try:
          a = float64(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f64_sub(_softfloat.i64_to_f64(other), self.v)
          elif isinstance(other, (float)):
              a.v = _softfloat.f64_sub(_softfloat.convertDoubleToF64(other), self.v)
          else:
              a.v = _softfloat.f64_sub(other.v, self.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for -: float64 and ",other.type())  
   def __mul__(self, other):
       try:
          a = float64(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f64_mul(self.v, _softfloat.i64_to_f64(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f64_mul(self.v, _softfloat.convertDoubleToF64(other))
          else:
              a.v = _softfloat.f64_mul(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for *: float64 and ",other.type())   
   def __rmul__(self, other):
       return self.__mul__(other)  
   def __div__(self, other):
       try:
          a = float64(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f64_div(self.v, _softfloat.i64_to_f64(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f64_div(self.v, _softfloat.convertDoubleToF64(other))
          else:
              a.v = _softfloat.f64_div(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for /: float64 and ",other.type())   
   def __truediv__(self, other):
       return self.__div__(other)      
   def __rdiv__(self, other):
       try:
          a = float64(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f64_div(_softfloat.i64_to_f64(other), self.v)
          elif isinstance(other, (float)):
              a.v = _softfloat.f64_div(_softfloat.convertDoubleToF64(other), self.v)
          else:
              a.v = _softfloat.f64_div(other.v, self.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for *: float64 and ",other.type())  
   def __rtruediv__(self, other):
       return self.__rdiv__(other)
   def __eq__(self, other):
       try:
          if isinstance(other, (int)):
              return _softfloat.f64_eq(self.v, _softfloat.i64_to_f64(other))
          elif isinstance(other, (float)):
              return _softfloat.f64_eq(self.v, _softfloat.convertDoubleToF64(other))
          else:
              return _softfloat.f64_eq(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for ==: float64 and ",other.type())   
   def __ne__(self, other):
       try:                                                                                     
          if isinstance(other, (int)):
              return not(_softfloat.f64_eq(self.v, _softfloat.i64_to_f64(other)))
          elif isinstance(other, (float)):
              return not(_softfloat.f64_eq(self.v, _softfloat.convertDoubleToF64(other)))
          else:
              return not(_softfloat.f64_eq(self.v, other.v))
       except TypeError:
          print("TypeError: Unsupported operand type(s) for !=: float64 and ",other.type())
   def __le__(self, other):
       try:        
          if isinstance(other, (int)):
              return _softfloat.f64_le(self.v, _softfloat.i64_to_f64(other))
          elif isinstance(other, (float)):
              return _softfloat.f64_le(self.v, _softfloat.convertDoubleToF64(other))
          else:
              return _softfloat.f64_le(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for <=: float64 and ",other.type()) 
   def __lt__(self, other):
       try:   
          if isinstance(other, (int)):
              return _softfloat.f64_lt(self.v, _softfloat.i64_to_f64(other))
          elif isinstance(other, (float)):
              return _softfloat.f64_lt(self.v, _softfloat.convertDoubleToF64(other))
          else:
              return _softfloat.f64_lt(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for <: float64 and ",other.type())
   def __ge__(self, other):
       try:   
          if isinstance(other, (int)):
              return _softfloat.f64_le(_softfloat.i64_to_f64(other), self.v)
          elif isinstance(other, (float)):
              return _softfloat.f64_le(_softfloat.convertDoubleToF64(other), self.v)
          else:
              return _softfloat.f64_le(other.v, self.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for >=: float64 and ",other.type())
   def __gt__(self, other):
       try:   
          if isinstance(other, (int)):
              return _softfloat.f64_lt(_softfloat.i64_to_f64(other), self.v)
          elif isinstance(other, (float)):
              return _softfloat.f64_lt(_softfloat.convertDoubleToF64(other), self.v)
          else:
              return _softfloat.f64_lt(other.v, self.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for >: float64 and ",other.type())
   def __rshift__(self, other):
       a = float64(0)
       a.v = self.v.__rshift__(other)
       return a
   def __lshift__(self, other):
       a = float64(0)
       a.v = self.v.__lshift__(other)
       return a
   def __pos__(self):
       return self
   def __neg__(self):
       a = float64(0)
       a.v = self.v.__neg__()
       return a
   def __abs__(self):
       a = float64(0)
       a.v = self.v.__abs__()
       return a
   def __invert__(self):
       self.v = self.v.__invert__()
       return self   
   def __and__(self, other):
       a = float64(0)
       a.v = self.v.__and__(other.v)
       return a
   def __xor__(self, other):
       a = float64(0)
       a.v = self.v.__xor__(other.v)
       return a
   def __or__(self, other):
       a = float64(0)
       a.v = self.v.__or__(other.v)
       return a
   def fma(self, other1, other2):
       try:   
          a = float64(0)
          if isinstance(other1, (int)):
              if isinstance(other2, (int)):
                  a.v = _softfloat.f64_mulAdd(_softfloat.i64_to_f64(other1), _softfloat.i64_to_f64(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f64_mulAdd(_softfloat.i64_to_f64(other1), _softfloat.convertDoubleToF64(other2), self.v)
              else:
                  a.v = _softfloat.f64_mulAdd(_softfloat.i64_to_f64(other1), other2.v, self.v)
          elif isinstance(other1, (float)):
              if isinstance(other2, (int)):
                  a.v = _softfloat.f64_mulAdd(_softfloat.convertDoubleToF64(other1), _softfloat.i64_to_f64(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f64_mulAdd(_softfloat.convertDoubleToF64(other1), _softfloat.convertDoubleToF64(other2), self.v)
              else:
                  a.v = _softfloat.f64_mulAdd(_softfloat.convertDoubleToF64(other1), other2.v, self.v)
          else:
              if isinstance(other2, (int)):
                  a.v = _softfloat.f64_mulAdd(self.v, other1.v, _softfloat.i64_to_f64(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f64_mulAdd(other1.v, _softfloat.convertDoubleToF64(other2), self.v)
              else:
                  a.v = _softfloat.f64_mulAdd(other1.v, other2.v, self.v)   
          return a
       except TypeError:
          print("TypeError: Unsupported fused operand (fma) among mixed precison float types")
   def toFloat16(self):
       a = float16(0)
       a.v = _softfloat.f64_to_f16(self.v)
       return a
   def toFloat32(self):
       a = float32(0)
       a.v = _softfloat.f64_to_f32(self.v)
       return a
   def toRInt(self):
       return _softfloat.f64_to_i64(self.v)
   def toInt(self):
       return int(struct.unpack('>d', struct.pack('>Q', self.v.v))[0])
   def rint(self):
       self.v = _softfloat.f64_roundToInt(self.v)
       return self
   def sqrt(self):
       self.v = _softfloat.f64_sqrt(self.v)
       return self
   def __repr__(self):
       a = float(struct.unpack('>d', struct.pack('>Q', self.v.v))[0])
       return str(a)
   def __str__(self):
       a = float(struct.unpack('>d', struct.pack('>Q', self.v.v))[0])
       return str(a)
   def __int__(self):
       return int(struct.unpack('>d', struct.pack('>Q', self.v.v))[0])
   def __float__(self):
       return struct.unpack('>d', struct.pack('>Q', self.v.v))[0]
   def fromBits(self, value):
       self.v.fromBits(value)
   def toBinary(self):
       self.v.toBits()
   def toBinaryFormatted(self):
       print(convertToColor(self.v.v, 64, 11))
   def toHex(self):
       self.v.toHex()


class float16:
   def __init__(self, value=None, bits=None):
       try:
           if bits is not None:
               if isinstance(bits, (int)):
                   self.v = float16_t()
                   self.v.v = bits & 0xFFFF
               else:
                   raise Exception("Bits can only be set with integer values")
           else:
               if value is None:
                   self.v = float16_t()
                   self.v.v = 0
               elif isinstance(value, (int)):
                   self.v = _softfloat.i64_to_f16(value)
               else:
                   self.v = _softfloat.convertDoubleToF16(value)
       except Exception as error:
           print(repr(error))
   def type(self):
       return 'float16'
   def __add__(self, other):
       try:
          a = float16(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f16_add(self.v, _softfloat.i64_to_f16(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f16_add(self.v, _softfloat.convertDoubleToF16(other))
          else:
              a.v = _softfloat.f16_add(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for +: float16 and ",other.type())     
   def __radd__(self, other):
       return self.__add__(other)  
   def __sub__(self, other):
       try:
          a = float16(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f16_sub(self.v, _softfloat.i64_to_f16(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f16_sub(self.v, _softfloat.convertDoubleToF16(other))
          else:
              a.v = _softfloat.f16_sub(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for -: float16 and ",other.type()) 
   def __rsub__(self, other):
       try:
          a = float16(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f16_sub(_softfloat.i64_to_f16(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f16_sub(_softfloat.convertDoubleToF16(other), self.v)
          else:
              a.v = _softfloat.f16_sub(other.v, self.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for -: float16 and ",other.type())  
   def __mul__(self, other):
       try:
          a = float16(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f16_mul(self.v, _softfloat.i64_to_f16(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f16_mul(self.v, _softfloat.convertDoubleToF16(other))
          else:
              a.v = _softfloat.f16_mul(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for *: float16 and ",other.type()) 
   def __rmul__(self, other):
       return self.__mul__(other)  
   def __div__(self, other):
       try:
          a = float16(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f16_div(self.v, _softfloat.i64_to_f16(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f16_div(self.v, _softfloat.convertDoubleToF16(other))
          else:
              a.v = _softfloat.f16_div(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for /: float16 and ",other.type()) 
   def __truediv__(self, other):
       return self.__div__(other)
   def __rdiv__(self, other):
       try:
          a = float16(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f16_div(_softfloat.i64_to_f16(other), self.v)
          elif isinstance(other, (float)):
              a.v = _softfloat.f16_div(_softfloat.convertDoubleToF16(other), self.v)
          else:
              a.v = _softfloat.f16_div(other.v, self.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for /: float16 and ",other.type()) 
   def __rtruediv__(self, other):
       return self.__rdiv__(other)
   def __eq__(self, other):
       try:
          if isinstance(other, (int)):
              return _softfloat.f16_eq(self.v, _softfloat.i64_to_f16(other))
          elif isinstance(other, (float)):
              return _softfloat.f16_eq(self.v, _softfloat.convertDoubleToF16(other))
          else:
              return _softfloat.f16_eq(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for ==: float16 and ",other.type()) 
   def __ne__(self, other):  
       try:                                                                                          
          if isinstance(other, (int)):
              return not(_softfloat.f16_eq(self.v, _softfloat.i64_to_f16(other)))
          elif isinstance(other, (float)):
              return not(_softfloat.f16_eq(self.v, _softfloat.convertDoubleToF16(other)))
          else:
              return not(_softfloat.f16_eq(self.v, other.v))
       except TypeError:
          print("TypeError: Unsupported operand type(s) for !=: float16 and ",other.type())
   def __le__(self, other):
       try:             
          if isinstance(other, (int)):
              return _softfloat.f16_le(self.v, _softfloat.i64_to_f16(other))
          elif isinstance(other, (float)):
              return _softfloat.f16_le(self.v, _softfloat.convertDoubleToF16(other))
          else:
              return _softfloat.f16_le(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for <=: float16 and ",other.type()) 
   def __lt__(self, other):
       try:             
          if isinstance(other, (int)):
              return _softfloat.f16_lt(self.v, _softfloat.i64_to_f16(other))
          elif isinstance(other, (float)):
              return _softfloat.f16_lt(self.v, _softfloat.convertDoubleToF16(other))
          else:
              return _softfloat.f16_lt(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for <: float16 and ",other.type()) 
   def __ge__(self, other):
       try:    
          if isinstance(other, (int)):
              return _softfloat.f16_le(_softfloat.i64_to_f16(other), self.v)
          elif isinstance(other, (float)):
              return _softfloat.f16_le(_softfloat.convertDoubleToF16(other), self.v)
          else:
              return _softfloat.f16_le(other.v, self.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for >=: float16 and ",other.type())  
   def __gt__(self, other):
       if isinstance(other, (int)):
           return _softfloat.f16_lt(_softfloat.i64_to_f16(other), self.v)
       elif isinstance(other, (float)):
           return _softfloat.f16_lt(_softfloat.convertDoubleToF16(other), self.v)
       else:
           return _softfloat.f16_lt(other.v, self.v)
   def __rshift__(self, other):
       a = float16(0)
       a.v = self.v.__rshift__(other)
       return a
   def __lshift__(self, other):
       a = float16(0)
       a.v = self.v.__lshift__(other)
       return a
   def __pos__(self):
       return self
   def __neg__(self):
       a = float16(0)
       a.v = self.v.__neg__()
       return a
   def __abs__(self):
       a = float16(0)
       a.v = self.v.__abs__()
       return a
   def __invert__(self):
       self.v = self.v.__invert__()
       return self   
   def __and__(self, other):
       a = float16(0)
       a.v = self.v.__and__(other.v)
       return a
   def __xor__(self, other):
       a = float16(0)
       a.v = self.v.__xor__(other.v)
       return a
   def __or__(self, other):
       a = float16(0)
       a.v = self.v.__or__(other.v)
       return a
   def fma(self, other1, other2):
       try:
          a = float16(0)
          if isinstance(other1, (int)):
              if isinstance(other2, (int)):
                  a.v = _softfloat.f16_mulAdd(_softfloat.i64_to_f16(other1), _softfloat.i64_to_f16(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f16_mulAdd(_softfloat.i64_to_f16(other1), _softfloat.convertDoubleToF16(other2), self.v)
              else:
                  a.v = _softfloat.f16_mulAdd(_softfloat.i64_to_f16(other1), other2.v, self.v)
          elif isinstance(other1, (float)):
              if isinstance(other2, (int)):
                  a.v = _softfloat.f16_mulAdd(_softfloat.convertDoubleToF16(other1), _softfloat.i64_to_f16(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f16_mulAdd(_softfloat.convertDoubleToF16(other1), _softfloat.convertDoubleToF16(other2), self.v)
              else:
                  a.v = _softfloat.f16_mulAdd(_softfloat.convertDoubleToF16(other1), other2.v, self.v)
          else:
              if isinstance(other2, (int)):
                  a.v = _softfloat.f16_mulAdd(other1.v, _softfloat.i64_to_f16(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f16_mulAdd(other1.v, _softfloat.convertDoubleToF16(other2), self.v)
              else:
                  a.v = _softfloat.f16_mulAdd(other1.v, other2.v, self.v)
          return a
       except TypeError:
          print("TypeError: Unsupported fused operand (fma) among mixed precison float types")
   def toFloat64(self):
       a = float64(0)
       a.v = _softfloat.f16_to_f64(self.v)
       return a
   def toFloat32(self):
       a = float32(0)
       a.v = _softfloat.f16_to_f32(self.v)
       return a
   def toRInt(self):
       return _softfloat.f16_to_i64(self.v)
   def toInt(self):
       return int(_softfloat.convertF16ToDouble(self.v))
   def rint(self):
       self.v = _softfloat.f16_roundToInt(self.v)
       return self
   def sqrt(self):
       self.v = _softfloat.f16_sqrt(self.v)
       return self
   def __repr__(self):
       a = float(_softfloat.convertF16ToDouble(self.v))
       return str(a)
   def __str__(self):
       a = float(_softfloat.convertF16ToDouble(self.v))
       return str(a)
   def __int__(self):
       return int(_softfloat.convertF16ToDouble(self.v))
   def __float__(self):
       return float(_softfloat.convertF16ToDouble(self.v))
   def fromBits(self, value):
       self.v.fromBits(value)
   def toBinary(self):
       self.v.toBits()
   def toBinaryFormatted(self):
       print(convertToColor(self.v.v, 16, 5))
   def toHex(self):
       self.v.toHex()



class float32:
   def __init__(self, value=None, bits=None):
       try:
           if bits is not None:
               if isinstance(bits, (int)):
                   self.v = float32_t()
                   self.v.v = bits & 0xFFFFFFFF
               else:
                   raise Exception("Bits can only be set with integer values")
           else:
               if value is None:
                   self.v = float32_t()
                   self.v.v = 0
               elif isinstance(value, (int)):
                   self.v = _softfloat.i64_to_f32(value)
               else:
                   self.v = _softfloat.convertDoubleToF32(value)
       except Exception as error:
           print(repr(error));
   def type(self):
       return 'float32'
   def __add__(self, other):
       try:
          a = float32(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f32_add(self.v, _softfloat.i64_to_f32(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f32_add(self.v, _softfloat.convertDoubleToF32(other))
          else:
              a.v = _softfloat.f32_add(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for +: float32 and ",other.type())     
   def __radd__(self, other):
       return self.__add__(other)  
   def __sub__(self, other):
       try:
          a = float32(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f32_sub(self.v, _softfloat.i64_to_f32(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f32_sub(self.v, _softfloat.convertDoubleToF32(other))
          else:
              a.v = _softfloat.f32_sub(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for -: float32 and ",other.type())  
   def __rsub__(self, other):
       try:
          a = float32(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f32_sub(_softfloat.i64_to_f32(other), self.v)
          elif isinstance(other, (float)):
              a.v = _softfloat.f32_sub(_softfloat.convertDoubleToF32(other), self.v)
          else:
              a.v = _softfloat.f32_sub(other.v, self.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for -: float32 and ",other.type())    
   def __mul__(self, other):
       try:
          a = float32(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f32_mul(self.v, _softfloat.i64_to_f32(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f32_mul(self.v, _softfloat.convertDoubleToF32(other))
          else:
              a.v = _softfloat.f32_mul(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for *: float32 and ",other.type())     
   def __rmul__(self, other):
       return self.__mul__(other)  
   def __div__(self, other):
       try:
          a = float32(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f32_div(self.v, _softfloat.i64_to_f32(other))
          elif isinstance(other, (float)):
              a.v = _softfloat.f32_div(self.v, _softfloat.convertDoubleToF32(other))
          else:
              a.v = _softfloat.f32_div(self.v, other.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for /: float32 and ",other.type())   
   def __truediv__(self, other):
       return self.__div__(other)
   def __rdiv__(self, other):
       try:
          a = float32(0)
          if isinstance(other, (int)):
              a.v = _softfloat.f32_div(_softfloat.i64_to_f32(other), self.v)
          elif isinstance(other, (float)):
              a.v = _softfloat.f32_div(_softfloat.convertDoubleToF32(other), self.v)
          else:
              a.v = _softfloat.f32_div(other.v, self.v)
          return a
       except TypeError:
          print("TypeError: Unsupported operand type(s) for /: float32 and ",other.type())     
   def __rtruediv__(self, other):
       return self.__rdiv__(other)
   def __eq__(self, other):
       try:
          if isinstance(other, (int)):
              return _softfloat.f32_eq(self.v, _softfloat.i64_to_f32(other))
          elif isinstance(other, (float)):
              return _softfloat.f32_eq(self.v, _softfloat.convertDoubleToF32(other))
          else:
              return _softfloat.f32_eq(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for ==: float32 and ",other.type())     
   def __ne__(self, other):  
       try:                                                                                
          if isinstance(other, (int)):
              return not(_softfloat.f32_eq(self.v, _softfloat.i64_to_f32(other)))
          elif isinstance(other, (float)):
              return not(_softfloat.f32_eq(self.v, _softfloat.convertDoubleToF32(other)))
          else:
              return not(_softfloat.f32_eq(self.v, other.v))
       except TypeError:
          print("TypeError: Unsupported operand type(s) for !=: float32 and ",other.type())    
   def __le__(self, other):
       try:
          if isinstance(other, (int)):
              return _softfloat.f32_le(self.v, _softfloat.i64_to_f32(other))
          elif isinstance(other, (float)):
              return _softfloat.f32_le(self.v, _softfloat.convertDoubleToF32(other))
          else:
              return _softfloat.f32_le(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for <=: float32 and ",other.type())  
   def __lt__(self, other):
       try:
          if isinstance(other, (int)):
              return _softfloat.f32_lt(self.v, _softfloat.i64_to_f32(other))
          elif isinstance(other, (float)):
              return _softfloat.f32_lt(self.v, _softfloat.convertDoubleToF32(other))
          else:
              return _softfloat.f32_lt(self.v, other.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for <: float32 and ",other.type())  
   def __ge__(self, other):
       try:
          if isinstance(other, (int)):
              return _softfloat.f32_le(_softfloat.i64_to_f32(other), self.v)
          elif isinstance(other, (float)):
              return _softfloat.f32_le(_softfloat.convertDoubleToF32(other), self.v)
          else:
              return _softfloat.f32_le(other.v, self.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for >=: float32 and ",other.type())  
   def __gt__(self, other):
       try:
          if isinstance(other, (int)):
              return _softfloat.f32_lt(_softfloat.i64_to_f32(other), self.v)
          elif isinstance(other, (float)):
              return _softfloat.f32_lt(_softfloat.convertDoubleToF32(other), self.v)
          else:
              return _softfloat.f32_lt(other.v, self.v)
       except TypeError:
          print("TypeError: Unsupported operand type(s) for >: float32 and ",other.type())  
   def __rshift__(self, other):
       a = float32(0)
       a.v = self.v.__rshift__(other)
       return a
   def __lshift__(self, other):
       a = float32(0)
       a.v = self.v.__lshift__(other)
       return a
   def __pos__(self):
       return self
   def __neg__(self):
       a = float32(0)
       a.v = self.v.__neg__()
       return a
   def __abs__(self):
       a = float32(0)
       a.v = self.v.__abs__()
       return a
   def __invert__(self):
       self.v = self.v.__invert__()
       return self   
   def __and__(self, other):
       a = float32(0)
       a.v = self.v.__and__(other.v)
       return a
   def __xor__(self, other):
       a = float32(0)
       a.v = self.v.__xor__(other.v)
       return a
   def __or__(self, other):
       a = float32(0)
       a.v = self.v.__or__(other.v)
       return a
   def fma(self, other1, other2):
       try:
          a = float32(0)
          if isinstance(other1, (int)):
              if isinstance(other2, (int)):
                  a.v = _softfloat.f32_mulAdd(_softfloat.i64_to_f32(other1), _softfloat.i64_to_f32(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f32_mulAdd(_softfloat.i64_to_f32(other1), _softfloat.convertDoubleToF32(other2), self.v)
              else:
                  a.v = _softfloat.f32_mulAdd(_softfloat.i64_to_f32(other1), other2.v, self.v)
          elif isinstance(other1, (float)):
              if isinstance(other2, (int)):
                  a.v = _softfloat.f32_mulAdd(_softfloat.convertDoubleToF32(other1), _softfloat.i64_to_f32(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f32_mulAdd(_softfloat.convertDoubleToF32(other1), _softfloat.convertDoubleToF32(other2), self.v)
              else:
                  a.v = _softfloat.f32_mulAdd(_softfloat.convertDoubleToF32(other1), other2.v, self.v)
          else:
              if isinstance(other2, (int)):
                  a.v = _softfloat.f32_mulAdd(other1.v, _softfloat.i64_to_f32(other2), self.v)
              elif isinstance(other2, (float)):
                  a.v = _softfloat.f32_mulAdd(other1.v, _softfloat.convertDoubleToF32(other2), self.v)
              else:
                  a.v = _softfloat.f32_mulAdd(other1.v, other2.v, self.v)   
          return a
       except TypeError:
          print("TypeError: Unsupported fused operand (fma) among mixed precison float types")
   def toFloat64(self):
       a = float64(0)
       a.v = _softfloat.f32_to_f64(self.v)
       return a
   def toFloat16(self):
       a = float16(0)
       a.v = _softfloat.f32_to_f16(self.v)
       return a
   def toRInt(self):
       return _softfloat.f32_to_i64(self.v)
   def toInt(self):
       return int(_softfloat.convertF32ToDouble(self.v))
   def rint(self):
       self.v = _softfloat.f32_roundToInt(self.v)
       return self
   def sqrt(self):
       self.v = _softfloat.f32_sqrt(self.v)
       return self
   def __repr__(self):
       a = float(_softfloat.convertF32ToDouble(self.v))
       return str(a)
   def __str__(self):
       a = float(_softfloat.convertF32ToDouble(self.v))
       return str(a)
   def __int__(self):
       return int(_softfloat.convertF32ToDouble(self.v))
   def __float__(self):
       return float(_softfloat.convertF32ToDouble(self.v))
   def fromBits(self, value):
       self.v.fromBits(value)
   def toBinary(self):
       self.v.toBits()
   def toBinaryFormatted(self):
       print(convertToColor(self.v.v, 32, 8))
   def toHex(self):
       self.v.toHex()


%}



%extend float64_t {
    float64_t init() {
        float64_t a;
        return a;
    }
    void fromBits(uint64_t bits) {
        $self->v = bits;
    }
    void toBits() {
	printBinary(((uint64_t*)&$self->v),64);
    }
    void toHex() {
	printHex($self->v);
    }
    int toInt() {
	return $self->v;
    }
    float64_t __rshift__(int n) {
       float64_t a; 
       a.v = ($self->v >> n);
       return a;
    }
    float64_t __lshift__(int n) {
       float64_t a; 
       a.v = ($self->v << n);
       return a;
    }
    float64_t __invert__() {
       $self->v = ~($self->v);
       return *self;
    }
    float64_t __neg__() {
       float64_t a;
       if ($self->v >> 63)
          a.v = ($self->v) ^ 0x8000000000000000;
       else       
          a.v = ($self->v) | 0x8000000000000000;
       return a;
    }
    float64_t __abs__() {
       float64_t a;
       if (($self->v)>>63) a.v = ($self->v) ^ 0x8000000000000000;
       else a.v = $self->v;
       return a;
    }
    float64_t __and__(float64_t other ){
		float64_t a;
        a.v = $self->v & other.v;
        return a;
    }
    float64_t __xor__(float64_t other ){
		float64_t a;
        a.v = $self->v ^ other.v;
        return a;
    }
    float64_t __or__(float64_t other ){
		float64_t a;
        a.v = $self->v | other.v;
        return a;
    }
    %pythoncode %{
       def __repr__(self):
           a = float(struct.unpack('>d', struct.pack('>Q', self.v))[0])
           return str(a)
       def __str__(self):
           a = float(struct.unpack('>d', struct.pack('>Q', self.v))[0])
           return str(a)
    %}
};



%extend float16_t {
    void fromBits(int bits) {
        $self->v = bits & 0xFFFF;
    }
    void toBits() {
	printBinary(((uint64_t*)&$self->v), 16);
    }
    void toHex() {
	printHex($self->v);
    }
    int toInt() {
	return $self->v;
    }
    float16_t __rshift__(int n) {
       float16_t a; 
       a.v = ($self->v >> n);
       return a;
    }
    float16_t __lshift__(int n) {
       float16_t a; 
       a.v = ($self->v << n) & 0xFFFF;
       return a;
    }
    float16_t __invert__() {
       $self->v = ~($self->v) & 0xFFFF;
       return *self;
    }
    float16_t __neg__() {
       float16_t a;
       if ($self->v >> 15)
          a.v = ($self->v) ^ 0x8000;
       else       
          a.v = ($self->v) | 0x8000;
       return a;
    }
    float16_t __abs__() {
       float16_t a;
       if (($self->v)>>15) a.v = ($self->v) ^ 0x8000;
       else a.v = $self->v;
       return a;
    }
    float16_t __and__(float16_t other ){
		float16_t a;
        a.v = $self->v & other.v;
        return a;
    }
    float16_t __xor__(float16_t other ){
		float16_t a;
        a.v = $self->v ^ other.v;
        return a;
    }
    float16_t __or__(float16_t other ){
		float16_t a;
        a.v = $self->v | other.v;
        return a;
    }
    %pythoncode %{
       def __repr__(self):
           a = float(_softfloat.convertF16ToDouble(self))
           return str(a)
       def __str__(self):
           a = float(_softfloat.convertF16ToDouble(self))
           return str(a)
    %}
	
};


%extend float32_t {
    void fromBits(long long int bits) {
        $self->v = bits & 0xFFFFFFFF;
    }
    void toBits() {
	printBinary(((uint64_t*)&$self->v), 32);
    }
    void toHex() {
	printHex($self->v);
    }
    int toInt() {
	return $self->v;
    }
    float32_t __rshift__(int n) {
       float32_t a; 
       a.v = ($self->v >> n);
       return a;
    }
    float32_t __lshift__(int n) {
       float32_t a; 
       a.v = ($self->v << n) & 0xFFFFFFFF;
       return a;
    }
    float32_t __invert__() {
       $self->v = ~($self->v) & 0xFFFFFFFF;
       return *self;
    }
    float32_t __neg__() {
       float32_t a;
       if ($self->v >> 31)
          a.v = ($self->v) ^ 0x80000000;
       else       
          a.v = ($self->v) | 0x80000000;
       return a;
    }
    float32_t __abs__() {
       float32_t a;
       if (($self->v)>>31) a.v = ($self->v) ^ 0x80000000;
       else a.v = $self->v;
       return a;
    }
    float32_t __and__(float32_t other ){
		float32_t a;
        a.v = $self->v & other.v;
        return a;
    }
    float32_t __xor__(float32_t other ){
		float32_t a;
        a.v = $self->v ^ other.v;
        return a;
    }
    float32_t __or__(float32_t other ){
		float32_t a;
        a.v = $self->v | other.v;
        return a;
    }
    %pythoncode %{
       def __repr__(self):
           a = float(_softfloat.convertF32ToDouble(self))
           return str(a)
       def __str__(self):
           a = float(_softfloat.convertF32ToDouble(self))
           return str(a)
    %}
};
